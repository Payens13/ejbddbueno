package controlador;

import modelo.Partida;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class PartidaJDBC {
    private final String SELECT_ALL = "SELECT * FROM Partidas";
    private final String SELECT_BY_ID = "SELECT * FROM Partidas WHERE id = ?";
    private final String SELECT_MAX_ID = "SELECT MAX(id) AS max_id FROM Partidas";
    private final String SELECT_MIN_ID = "SELECT MIN(id) AS min_id FROM Partidas";
    private final String SELECT_AVG_ID = "SELECT AVG(id) AS avg_id FROM Partidas";
    private final String SELECT_SUM_ID = "SELECT SUM(id) AS sum_id FROM Partidas";
    private final String INSERT_PARTIDA = "INSERT INTO Partidas (CampeonId, KDA, Victorias, Derrotas) VALUES(?, ?, ?, ?)";
    private final String DELETE_PARTIDA_BY_ID = "DELETE FROM Partidas WHERE id = ?";
    private final String UPDATE_PARTIDA_BY_ID = "UPDATE Partidas SET CampeonId = ?, KDA = ?, Victorias = ?, Derrotas = ? WHERE id = ?";

    Connection con;

    public PartidaJDBC(Connection con) {
        this.con = con;
    }

    public ArrayList<Partida> select_all() {
        ArrayList<Partida> lista_partidas = new ArrayList<Partida>();
        PreparedStatement stmt = null;
        ResultSet rs = null;
        try {
            stmt = con.prepareStatement(SELECT_ALL);
            rs = stmt.executeQuery();
            while (rs.next()) {
                int id = rs.getInt("id");
                int campeonId = rs.getInt("campeonId");
                String kda = rs.getString("kda");
                int victorias = rs.getInt("victorias");
                int derrotas = rs.getInt("derrotas");
                Partida p = new Partida(id, campeonId, kda, victorias, derrotas);
                lista_partidas.add(p);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                if (rs != null) rs.close();
                if (stmt != null) stmt.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return lista_partidas;
    }

    public Partida select_by_id(int id) {
        Partida p = null;
        PreparedStatement stmt = null;
        ResultSet rs = null;
        try {
            stmt = con.prepareStatement(SELECT_BY_ID);
            stmt.setInt(1, id);
            rs = stmt.executeQuery();
            if (rs.next()) {
                int campeonId = rs.getInt("campeonId");
                String kda = rs.getString("kda");
                int victorias = rs.getInt("victorias");
                int derrotas = rs.getInt("derrotas");
                p = new Partida(id, campeonId, kda, victorias, derrotas);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                if (rs != null) rs.close();
                if (stmt != null) stmt.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return p;
    }

    public int select_max_id() {
        int max_id = 0;
        PreparedStatement stmt = null;
        ResultSet rs = null;
        try {
            stmt = con.prepareStatement(SELECT_MAX_ID);
            rs = stmt.executeQuery();
            if (rs.next()) {
                max_id = rs.getInt("max_id");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                if (rs != null) rs.close();
                if (stmt != null) stmt.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return max_id;
    }

    public int select_min_id() {
        int min_id = 0;
        PreparedStatement stmt = null;
        ResultSet rs = null;
        try {
            stmt = con.prepareStatement(SELECT_MIN_ID);
            rs = stmt.executeQuery();
            if (rs.next()) {
                min_id = rs.getInt("min_id");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                if (rs != null) rs.close();
                if (stmt != null) stmt.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return min_id;
    }

    public double select_avg_id() {
        double avg_id = 0;
        PreparedStatement stmt = null;
        ResultSet rs = null;
        try {
            stmt = con.prepareStatement(SELECT_AVG_ID);
            rs = stmt.executeQuery();
            if (rs.next()) {
                avg_id = rs.getDouble("avg_id");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                if (rs != null) rs.close();
                if (stmt != null) stmt.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return avg_id;
    }

    public int select_sum_id() {
        int sum_id = 0;
        PreparedStatement stmt = null;
        ResultSet rs = null;
        try {
            stmt = con.prepareStatement(SELECT_SUM_ID);
            rs = stmt.executeQuery();
            if (rs.next()) {
                sum_id = rs.getInt("sum_id");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                if (rs != null) rs.close();
                if (stmt != null) stmt.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return sum_id;
    }

    public boolean insert_partida(Partida partida) {
        boolean salida = true;
        PreparedStatement stmt = null;
        try {
            stmt = con.prepareStatement(INSERT_PARTIDA);
            stmt.setInt(1, partida.getCampeonId());
            stmt.setString(2, partida.getKda());
            stmt.setInt(3, partida.getVictorias());
            stmt.setInt(4, partida.getDerrotas());
            int x = stmt.executeUpdate();
            if (x != 1) {
                salida = false;
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                if (stmt != null) stmt.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return salida;
    }

    public boolean delete_partida_by_id(int id) {
        boolean salida = true;
        PreparedStatement stmt = null;
        try {
            stmt = con.prepareStatement(DELETE_PARTIDA_BY_ID);
            stmt.setInt(1, id);
            int x = stmt.executeUpdate();
            if (x != 1) {
                salida = false;
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                if (stmt != null) stmt.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return salida;
    }

    public boolean update_partida_by_id(int id, Partida partida) {
        boolean salida = true;
        PreparedStatement stmt = null;
        try {
            stmt = con.prepareStatement(UPDATE_PARTIDA_BY_ID);
            stmt.setInt(1, partida.getCampeonId());
            stmt.setString(2, partida.getKda());
            stmt.setInt(3, partida.getVictorias());
            stmt.setInt(4, partida.getDerrotas());
            stmt.setInt(5, id);
            int x = stmt.executeUpdate();
            if (x != 1) {
                salida = false;
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                if (stmt != null) stmt.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return salida;
    }

    public ArrayList<Partida> select_by_campeon_id(int campeonId) {
        ArrayList<Partida> lista_partidas = new ArrayList<Partida>();
        PreparedStatement stmt = null;
        ResultSet rs = null;
        try {
            String SELECT_BY_CAMPEON_ID = "SELECT * FROM Partidas WHERE campeonId = ?";
            stmt = con.prepareStatement(SELECT_BY_CAMPEON_ID);
            stmt.setInt(1, campeonId);
            rs = stmt.executeQuery();
            while (rs.next()) {
                int id = rs.getInt("id");
                String kda = rs.getString("kda");
                int victorias = rs.getInt("victorias");
                int derrotas = rs.getInt("derrotas");
                Partida p = new Partida(id, campeonId, kda, victorias, derrotas);
                lista_partidas.add(p);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                if (rs != null) rs.close();
                if (stmt != null) stmt.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return lista_partidas;
    }

    public boolean update_kda_by_id(int id, String kda) {
        boolean salida = true;
        PreparedStatement stmt = null;
        try {
            String UPDATE_KDA_BY_ID = "UPDATE Partidas SET kda = ? WHERE id = ?";
            stmt = con.prepareStatement(UPDATE_KDA_BY_ID);
            stmt.setString(1, kda);
            stmt.setInt(2, id);
            int x = stmt.executeUpdate();
            if (x != 1) {
                salida = false;
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                if (stmt != null) stmt.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return salida;
    }

}

package controlador;

import modelo.Habilidad;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class HabilidadJDBC {
    private final String SELECT_ALL = "SELECT * FROM Habilidades";
    private final String SELECT_BY_ID = "SELECT * FROM Habilidades WHERE id = ?";
    private final String SELECT_BY_NOMBRE = "SELECT * FROM Habilidades WHERE nombre = ?";
    private final String SELECT_MAX_ID = "SELECT MAX(id) AS max_id FROM Habilidades";
    private final String SELECT_MIN_ID = "SELECT MIN(id) AS min_id FROM Habilidades";
    private final String SELECT_AVG_ID = "SELECT AVG(id) AS avg_id FROM Habilidades";
    private final String SELECT_SUM_ID = "SELECT SUM(id) AS sum_id FROM Habilidades";
    private final String INSERT_HABILIDAD = "INSERT INTO Habilidades (Nombre, Descripcion, CampeonId) VALUES(?, ?, ?)";
    private final String DELETE_HABILIDAD_BY_ID = "DELETE FROM Habilidades WHERE id = ?";
    private final String UPDATE_HABILIDAD_BY_ID = "UPDATE Habilidades SET Nombre = ?, Descripcion = ?, CampeonId = ? WHERE id = ?";
    private final String UPDATE_NOMBRE_BY_ID = "UPDATE Habilidades SET Nombre = ? WHERE id = ?";

    Connection con;

    public HabilidadJDBC(Connection con) {
        this.con = con;
    }

    public ArrayList<Habilidad> select_all() {
        ArrayList<Habilidad> lista_habilidades = new ArrayList<Habilidad>();
        PreparedStatement stmt = null;
        ResultSet rs = null;
        try {
            stmt = con.prepareStatement(SELECT_ALL);
            rs = stmt.executeQuery();
            while (rs.next()) {
                int id = rs.getInt("id");
                String nombre = rs.getString("nombre");
                String descripcion = rs.getString("descripcion");
                int campeonId = rs.getInt("campeonId");
                Habilidad h = new Habilidad(id, nombre, descripcion, campeonId);
                lista_habilidades.add(h);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                if (rs != null) rs.close();
                if (stmt != null) stmt.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return lista_habilidades;
    }

    public Habilidad select_by_id(int id) {
        Habilidad h = null;
        PreparedStatement stmt = null;
        ResultSet rs = null;
        try {
            stmt = con.prepareStatement(SELECT_BY_ID);
            stmt.setInt(1, id);
            rs = stmt.executeQuery();
            if (rs.next()) {
                String nombre = rs.getString("nombre");
                String descripcion = rs.getString("descripcion");
                int campeonId = rs.getInt("campeonId");
                h = new Habilidad(id, nombre, descripcion, campeonId);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                if (rs != null) rs.close();
                if (stmt != null) stmt.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return h;
    }

    public ArrayList<Habilidad> select_by_nombre(String nombre) {
        ArrayList<Habilidad> lista_habilidades = new ArrayList<Habilidad>();
        PreparedStatement stmt = null;
        ResultSet rs = null;
        try {
            stmt = con.prepareStatement(SELECT_BY_NOMBRE);
            stmt.setString(1, nombre);
            rs = stmt.executeQuery();
            while (rs.next()) {
                int id = rs.getInt("id");
                String descripcion = rs.getString("descripcion");
                int campeonId = rs.getInt("campeonId");
                Habilidad h = new Habilidad(id, nombre, descripcion, campeonId);
                lista_habilidades.add(h);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                if (rs != null) rs.close();
                if (stmt != null) stmt.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return lista_habilidades;
    }

    public int select_max_id() {
        int max_id = 0;
        PreparedStatement stmt = null;
        ResultSet rs = null;
        try {
            stmt = con.prepareStatement(SELECT_MAX_ID);
            rs = stmt.executeQuery();
            if (rs.next()) {
                max_id = rs.getInt("max_id");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                if (rs != null) rs.close();
                if (stmt != null) stmt.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return max_id;
    }

    public int select_min_id() {
        int min_id = 0;
        PreparedStatement stmt = null;
        ResultSet rs = null;
        try {
            stmt = con.prepareStatement(SELECT_MIN_ID);
            rs = stmt.executeQuery();
            if (rs.next()) {
                min_id = rs.getInt("min_id");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                if (rs != null) rs.close();
                if (stmt != null) stmt.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return min_id;
    }

    public double select_avg_id() {
        double avg_id = 0;
        PreparedStatement stmt = null;
        ResultSet rs = null;
        try {
            stmt = con.prepareStatement(SELECT_AVG_ID);
            rs = stmt.executeQuery();
            if (rs.next()) {
                avg_id = rs.getDouble("avg_id");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                if (rs != null) rs.close();
                if (stmt != null) stmt.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return avg_id;
    }

    public int select_sum_id() {
        int sum_id = 0;
        PreparedStatement stmt = null;
        ResultSet rs = null;
        try {
            stmt = con.prepareStatement(SELECT_SUM_ID);
            rs = stmt.executeQuery();
            if (rs.next()) {
                sum_id = rs.getInt("sum_id");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                if (rs != null) rs.close();
                if (stmt != null) stmt.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return sum_id;
    }

    public boolean insert_habilidad(Habilidad habilidad) {
        boolean salida = true;
        PreparedStatement stmt = null;
        try {
            stmt = con.prepareStatement(INSERT_HABILIDAD);
            stmt.setString(1, habilidad.getNombre());
            stmt.setString(2, habilidad.getDescripcion());
            stmt.setInt(3, habilidad.getCampeonId());
            int x = stmt.executeUpdate();
            if (x != 1) {
                salida = false;
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                if (stmt != null) stmt.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return salida;
    }

    public boolean delete_habilidad_by_id(int id) {
        boolean salida = true;
        PreparedStatement stmt = null;
        try {
            stmt = con.prepareStatement(DELETE_HABILIDAD_BY_ID);
            stmt.setInt(1, id);
            int x = stmt.executeUpdate();
            if (x != 1) {
                salida = false;
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                if (stmt != null) stmt.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return salida;
    }

    public boolean update_habilidad_by_id(int id, Habilidad habilidad) {
        boolean salida = true;
        PreparedStatement stmt = null;
        try {
            stmt = con.prepareStatement(UPDATE_HABILIDAD_BY_ID);
            stmt.setString(1, habilidad.getNombre());
            stmt.setString(2, habilidad.getDescripcion());
            stmt.setInt(3, habilidad.getCampeonId());
            stmt.setInt(4, id);
            int x = stmt.executeUpdate();
            if (x != 1) {
                salida = false;
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                if (stmt != null) stmt.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return salida;
    }

    public boolean update_nombre_by_id(int id, String nombre) {
        boolean salida = true;
        PreparedStatement stmt = null;
        try {
            stmt = con.prepareStatement(UPDATE_NOMBRE_BY_ID);
            stmt.setString(1, nombre);
            stmt.setInt(2, id);
            int x = stmt.executeUpdate();
            if (x != 1) {
                salida = false;
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                if (stmt != null) stmt.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return salida;
    }

    public ArrayList<Habilidad> select_by_campeon_id(int campeonId) {
        ArrayList<Habilidad> lista_habilidades = new ArrayList<Habilidad>();
        PreparedStatement stmt = null;
        ResultSet rs = null;
        try {
            String SELECT_BY_CAMPEON_ID = "SELECT * FROM Habilidades WHERE campeonId = ?";
            stmt = con.prepareStatement(SELECT_BY_CAMPEON_ID);
            stmt.setInt(1, campeonId);
            rs = stmt.executeQuery();
            while (rs.next()) {
                int id = rs.getInt("id");
                String nombre = rs.getString("nombre");
                String descripcion = rs.getString("descripcion");
                Habilidad h = new Habilidad(id, nombre, descripcion, campeonId);
                lista_habilidades.add(h);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                if (rs != null) rs.close();
                if (stmt != null) stmt.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return lista_habilidades;
    }

}

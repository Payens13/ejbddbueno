package modelo;

import java.util.ArrayList;

public interface HabilidadDAO {
    ArrayList<Habilidad> select_all();
    Habilidad select_by_id(int id);
    ArrayList<Habilidad> select_by_nombre(String nombre);
    int select_max_id();
    int select_min_id();
    double select_avg_id();
    int select_sum_id();
    boolean insert_habilidad(Habilidad habilidad);
    boolean delete_habilidad_by_id(int id);
    boolean update_habilidad_by_id(int id, Habilidad habilidad);
}

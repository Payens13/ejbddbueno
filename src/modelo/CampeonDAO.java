package modelo;

import java.util.ArrayList;

public interface CampeonDAO {
    ArrayList<Campeon> select_all();
    Campeon select_by_id(int id);
    ArrayList<Campeon> select_by_nombre(String nombre);
    int select_max_id();
    int select_min_id();
    double select_avg_id();
    int select_sum_id();
    boolean insert_campeon(Campeon campeon);
    boolean delete_campeon_by_id(int id);
    boolean update_campeon_by_id(int id, Campeon campeon);
    boolean update_nombre_by_id(int id, String nombre);
}

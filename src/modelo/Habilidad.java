package modelo;

public class Habilidad {
    private int id;
    private String nombre;
    private String descripcion;
    private int campeonId; // Clave foránea

    public Habilidad(int id, String nombre, String descripcion, int campeonId) {
        this.id = id;
        this.nombre = nombre;
        this.descripcion = descripcion;
        this.campeonId = campeonId;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public int getCampeonId() {
        return campeonId;
    }

    public void setCampeonId(int campeonId) {
        this.campeonId = campeonId;
    }

    @Override
    public String toString() {
        return "Habilidad{" +
                "id=" + id +
                ", nombre='" + nombre + '\'' +
                ", descripcion='" + descripcion + '\'' +
                ", campeonId=" + campeonId +
                '}';
    }
}
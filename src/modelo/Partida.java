package modelo;

public class Partida {
    private int id;
    private int campeonId; // Clave foránea
    private String kda;
    private int victorias;
    private int derrotas;

    public Partida(int id, int campeonId, String kda, int victorias, int derrotas) {
        this.id = id;
        this.campeonId = campeonId;
        this.kda = kda;
        this.victorias = victorias;
        this.derrotas = derrotas;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getCampeonId() {
        return campeonId;
    }

    public void setCampeonId(int campeonId) {
        this.campeonId = campeonId;
    }

    public String getKda() {
        return kda;
    }

    public void setKda(String kda) {
        this.kda = kda;
    }

    public int getVictorias() {
        return victorias;
    }

    public void setVictorias(int victorias) {
        this.victorias = victorias;
    }

    public int getDerrotas() {
        return derrotas;
    }

    public void setDerrotas(int derrotas) {
        this.derrotas = derrotas;
    }

    @Override
    public String toString() {
        return "Partida{" +
                "id=" + id +
                ", campeonId=" + campeonId +
                ", kda='" + kda + '\'' +
                ", victorias=" + victorias +
                ", derrotas=" + derrotas +
                '}';
    }
}
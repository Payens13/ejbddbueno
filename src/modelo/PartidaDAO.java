package modelo;

import java.util.ArrayList;

public interface PartidaDAO {
    ArrayList<Partida> select_all();
    Partida select_by_id(int id);
    int select_max_id();
    int select_min_id();
    double select_avg_id();
    int select_sum_id();
    boolean insert_partida(Partida partida);
    boolean delete_partida_by_id(int id);
    boolean update_partida_by_id(int id, Partida partida);
}

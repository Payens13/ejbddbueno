package vista;

import Utilidades.Conexion;
import controlador.CampeonJDBC;
import modelo.Campeon;
import java.sql.*;
import java.util.ArrayList;
import java.util.Scanner;

import controlador.HabilidadJDBC;
import controlador.PartidaJDBC;
import modelo.Habilidad;
import modelo.Partida;

public class Main {
    public static void main(String[] args) {
        Conexion c = new Conexion();
        Connection conn = null;
        try {
            System.out.println("Connecting to database...");
            conn = c.getConnection();
            CampeonJDBC controller = new CampeonJDBC(conn);
            // Crear instancias de HabilidadJDBC y PartidaJDBC utilizando la conexión
            HabilidadJDBC habilidadController = new HabilidadJDBC(conn);
            PartidaJDBC partidaController = new PartidaJDBC(conn);

            Scanner sc = new Scanner(System.in);
            String opcion = "";
            while (!opcion.equals("9")) {
                System.out.println("\nPor favor, introduce una opción:");
                System.out.println("1. Mostrar todos los campeones");
                System.out.println("2. Mostrar el campeón con ID 1");
                System.out.println("3. Mostrar los campeones con nombre 'Ahri'");
                System.out.println("4. Mostrar el ID más grande");
                System.out.println("5. Insertar un nuevo campeón");
                System.out.println("6. Actualizar el nombre del campeón con ID 999");
                System.out.println("7. Actualizar todos los parámetros de un campeón por su ID");
                System.out.println("8. Borrar el campeón por ID");
                System.out.println("9. Salir");
                System.out.println("10. Mostrar todas las habilidades");
                System.out.println("11. Mostrar las habilidades de un campeón por su ID");
                System.out.println("12. Insertar una nueva habilidad");
                System.out.println("13. Actualizar una habilidad por su ID");
                System.out.println("14. Borrar una habilidad por su ID");
                System.out.println("15. Mostrar todas las partidas");
                System.out.println("16. Mostrar las partidas de un campeón por su ID");
                System.out.println("17. Insertar una nueva partida");
                System.out.println("18. Actualizar una partida por su ID");
                System.out.println("19. Borrar una partida por su ID");
                opcion = sc.nextLine();

                switch (opcion) {

                    case "1":
                        // Mostrar todos los datos de la tabla Campeones
                        System.out.println("Mostrando todos los campeones:");
                        ArrayList<Campeon> lista = controller.select_all();
                        if (lista == null || lista.isEmpty()) {
                            System.out.println("No se encontraron campeones.");
                        } else {
                            for (Campeon champ : lista) {
                                System.out.println(champ);
                            }
                        }
                        break;

                    case "2":
                        // Mostrar los datos de un campeón por su ID
                        System.out.println("\nIntroduce la ID del campeón que quieres mostrar:");
                        int idMostrar = sc.nextInt();
                        sc.nextLine();
                        Campeon campeonPorId = controller.select_by_id(idMostrar);
                        if (campeonPorId == null) {
                            System.out.println("No se encontró el campeón con ID " + idMostrar + ".");
                        } else {
                            System.out.println(campeonPorId);
                        }
                        break;

                    case "3":
                        // Mostrar los datos de los campeones por su nombre
                        System.out.println("\nMostrando los campeones con nombre 'Ahri':");
                        ArrayList<Campeon> campeonesPorNombre = controller.select_by_nombre("Ahri");
                        if (campeonesPorNombre == null || campeonesPorNombre.isEmpty()) {
                            System.out.println("No se encontraron campeones con el nombre 'Ahri'.");
                        } else {
                            for (Campeon champ : campeonesPorNombre) {
                                System.out.println(champ);
                            }
                        }
                        break;
                    case "4":

                        // Mostrar el ID más grande en la tabla Campeones
                        System.out.println("\nEl ID más grande es: " + controller.select_max_id());
                        break;

                    case "5":
                        // Insertar un nuevo campeón en la tabla Campeones
                        System.out.println("\nInsertando un nuevo campeón:");
                        Campeon nuevoCampeon = new Campeon(999, "NuevoCampeon", "Asesino", "Ionia");
                        boolean insertado = controller.insert_campeon(nuevoCampeon);
                        if (insertado) {
                            System.out.println("El campeón ha sido insertado correctamente.");
                        } else {
                            System.out.println("Ha ocurrido un error al insertar el campeón.");
                        }
                        break;

                    case "6":
                        // Actualizar parámetro: Actualizar el nombre del campeón con ID 999
                        System.out.println("\nIntroduce la ID del campeón cuyo nombre quieres actualizar:");
                        int idActualizarNombre = sc.nextInt();
                        sc.nextLine();
                        System.out.println("Introduce el nuevo nombre:");
                        String nuevoNombre = sc.nextLine();
                        boolean nombreActualizado = controller.update_nombre_by_id(idActualizarNombre, nuevoNombre);
                        if (nombreActualizado) {
                            System.out.println("El nombre del campeón ha sido actualizado correctamente.");
                        } else {
                            System.out.println("Ha ocurrido un error al actualizar el nombre del campeón.");
                        }
                        break;

                    case "7":
                        //actualizar todos los parámetros de un campeón por su ID
                        Campeon campeonActualizado = new Campeon(999, "CampeonActualizado", "Tanque", "Demacia");
                        boolean actualizado = controller.update_campeon_by_id(999, campeonActualizado);
                        if (actualizado) {
                            System.out.println("El campeón ha sido actualizado correctamente.");
                        } else {
                            System.out.println("Ha ocurrido un error al actualizar el campeón.");
                        }
                        break;

                    case "8":
                        //Borrar por clave primaria (ID)
                        System.out.println("Introduce la ID del campeón que quieres borrar:");
                        int idBorrar = sc.nextInt();
                        sc.nextLine();
                        boolean borrado = controller.delete_campeon_by_id(idBorrar);
                        if (borrado) {
                            System.out.println("El campeón ha sido borrado correctamente.");
                        } else {
                            System.out.println("Ha ocurrido un error al borrar el campeón.");
                        }
                        break;

                    case "9":
                        System.out.println("Saliendo...");
                        break;
                    default:
                        System.out.println("Opción no reconocida.");
                        break;

                    case "10":
                        // Mostrar todas las habilidades
                        System.out.println("Mostrando todas las habilidades:");
                        ArrayList<Habilidad> habilidades = habilidadController.select_all();
                        if (habilidades == null || habilidades.isEmpty()) {
                            System.out.println("No se encontraron habilidades.");
                        } else {
                            for (Habilidad habilidad : habilidades) {
                                System.out.println(habilidad);
                            }
                        }
                        break;

                    case "11":
                        // Mostrar las habilidades de un campeón por su ID
                        System.out.println("\nIntroduce la ID del campeón cuyas habilidades quieres mostrar:");
                        int idCampeonHabilidades = sc.nextInt();
                        sc.nextLine();
                        ArrayList<Habilidad> habilidadesPorCampeon = habilidadController.select_by_campeon_id(idCampeonHabilidades);
                        if (habilidadesPorCampeon == null || habilidadesPorCampeon.isEmpty()) {
                            System.out.println("No se encontraron habilidades para el campeón con ID " + idCampeonHabilidades + ".");
                        } else {
                            for (Habilidad habilidad : habilidadesPorCampeon) {
                                System.out.println(habilidad);
                            }
                        }
                        break;

                    case "12":
                        // Insertar una nueva habilidad
                        System.out.println("\nInsertando una nueva habilidad:");
                        Habilidad nuevaHabilidad = new Habilidad(999, "NuevaHabilidad", "Descripción", 1);
                        boolean habilidadInsertada = habilidadController.insert_habilidad(nuevaHabilidad);
                        if (habilidadInsertada) {
                            System.out.println("La habilidad ha sido insertada correctamente.");
                        } else {
                            System.out.println("Ha ocurrido un error al insertar la habilidad.");
                        }
                        break;

                    case "13":
                        // Actualizar una habilidad por su ID
                        System.out.println("\nIntroduce la ID de la habilidad que quieres actualizar:");
                        int idActualizarHabilidad = sc.nextInt();
                        sc.nextLine();
                        System.out.println("Introduce el nuevo nombre:");
                        String nuevoNombreHabilidad = sc.nextLine();
                        boolean habilidadActualizada = habilidadController.update_nombre_by_id(idActualizarHabilidad, nuevoNombreHabilidad);
                        if (habilidadActualizada) {
                            System.out.println("La habilidad ha sido actualizada correctamente.");
                        } else {
                            System.out.println("Ha ocurrido un error al actualizar la habilidad.");
                        }
                        break;

                    case "14":
                        // Borrar una habilidad por su ID
                        System.out.println("Introduce la ID de la habilidad que quieres borrar:");
                        int idBorrarHabilidad = sc.nextInt();
                        sc.nextLine();
                        boolean habilidadBorrada = habilidadController.delete_habilidad_by_id(idBorrarHabilidad);
                        if (habilidadBorrada) {
                            System.out.println("La habilidad ha sido borrada correctamente.");
                        } else {
                            System.out.println("Ha ocurrido un error al borrar la habilidad.");
                        }
                        break;

                    case "15":
                        // Mostrar todas las partidas
                        System.out.println("Mostrando todas las partidas:");
                        ArrayList<Partida> partidas = partidaController.select_all();
                        if (partidas == null || partidas.isEmpty()) {
                            System.out.println("No se encontraron partidas.");
                        } else {
                            for (Partida partida : partidas) {
                                System.out.println(partida);
                            }
                        }
                        break;

                    case "16":
                        // Mostrar las partidas de un campeón por su ID
                        System.out.println("\nIntroduce la ID del campeón cuyas partidas quieres mostrar:");
                        int idCampeonPartidas = sc.nextInt();
                        sc.nextLine();
                        ArrayList<Partida> partidasPorCampeon = partidaController.select_by_campeon_id(idCampeonPartidas);
                        if (partidasPorCampeon == null || partidasPorCampeon.isEmpty()) {
                            System.out.println("No se encontraron partidas para el campeón con ID " + idCampeonPartidas + ".");
                        } else {
                            for (Partida partida : partidasPorCampeon) {
                                System.out.println(partida);
                            }
                        }
                        break;

                    case "17":
                        // Insertar una nueva partida
                        System.out.println("\nInsertando una nueva partida:");
                        Partida nuevaPartida = new Partida(999, 1, "10/2/5", 1, 0);
                        boolean partidaInsertada = partidaController.insert_partida(nuevaPartida);
                        if (partidaInsertada) {
                            System.out.println("La partida ha sido insertada correctamente.");
                        } else {
                            System.out.println("Ha ocurrido un error al insertar la partida.");
                        }
                        break;

                    case "18":
                        // Actualizar una partida por su ID
                        System.out.println("\nIntroduce la ID de la partida que quieres actualizar:");
                        int idActualizarPartida = sc.nextInt();
                        sc.nextLine();
                        System.out.println("Introduce el nuevo KDA:");
                        String nuevoKda = sc.nextLine();
                        boolean partidaActualizada = partidaController.update_kda_by_id(idActualizarPartida, nuevoKda);
                        if (partidaActualizada) {
                            System.out.println("La partida ha sido actualizada correctamente.");
                        } else {
                            System.out.println("Ha ocurrido un error al actualizar la partida.");
                        }
                        break;

                    case "19":
                        // Borrar una partida por su ID
                        System.out.println("Introduce la ID de la partida que quieres borrar:");
                        int idBorrarPartida = sc.nextInt();
                        sc.nextLine();
                        boolean partidaBorrada = partidaController.delete_partida_by_id(idBorrarPartida);
                        if (partidaBorrada) {
                            System.out.println("La partida ha sido borrada correctamente.");
                        } else {
                            System.out.println("Ha ocurrido un error al borrar la partida.");
                        }
                        break;


                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (conn != null)
                    conn.close();
            } catch (SQLException se) {
                se.printStackTrace();
            }
        }
        System.out.println("-20 LPs. Cerrando sesión...");
    }
}
